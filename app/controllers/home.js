var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  Article = mongoose.model('Article');

module.exports = function (app) {
  app.use('/', router);
};

router.get('/', function (req, res, next) {
  
  var article = new Article({
    title: 'title',
    url: 'baidu.com',
    text: new Date().valueOf()
  });
  article.save(function (err, article) {
    console.log(article);
    console.log(err);
  });
  
  Article.find(function (err, articles) {
    console.log(articles);
    if (err) return next(err);
    res.render('index', {
      title: '攒米',
      articles: articles
    });
  });
});
